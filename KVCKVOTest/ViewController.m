//
//  ViewController.m
//  KVCKVOTest
//
//  Created by HuangXJ on 2017/6/10.
//  Copyright © 2017年 www.epoint.com.cn Inc. All rights reserved.
//

#import "ViewController.h"
#import "KVCTest.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // KVC 相关测试代码
    [self KVCTest];
    
    
}

- (void)KVCTest {
    KVCTest *kvc = [KVCTest new];
    [kvc setValue:@"huangxj" forKey:@"name"];
    NSLog(@"kvc.name = %@", kvc.name);
    
    // .m 中属性设置与读取
    [kvc setValue:@"man" forKey:@"sex"];
    NSString *sex = [kvc valueForKey:@"sex"];
    NSLog(@"kvc.sex = %@", sex);
    
    // .m 中readOnly属性设置与读取
    [kvc setValue:@"china" forKey:@"country"];
    NSString *country = [kvc valueForKey:@"country"];
    NSLog(@"kvc.country = %@", country);
    
    // .m 中实例变量设置与读取
    [kvc setValue:@"jiangsu" forKey:@"province"];
    NSString *province = [kvc valueForKey:@"province"];
    NSLog(@"kvc.province = %@", province);
    
    // 非对象类型
    [kvc setValue:@27 forKey:@"age"];
    NSNumber *age = [kvc valueForKey:@"age"];
    NSLog(@"kvc.age = %zd", [age unsignedIntegerValue]);
    
    // key路径设置
    [kvc setValue:@"blue" forKeyPath:@"car.carColor"];
    NSString *color = [kvc valueForKeyPath:@"car.carColor"];
    NSLog(@"kvc.car.carColor = %@", color);
}

@end
