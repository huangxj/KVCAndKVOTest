//
//  KVCTest.m
//  KVCKVOTest
//
//  Created by HuangXJ on 2017/6/10.
//  Copyright © 2017年 www.epoint.com.cn Inc. All rights reserved.
//

#import "KVCTest.h"
#import "Car.h"

@interface KVCTest () {
    NSString *_province;
}
@property (nonatomic, strong) NSString *sex;

@property (nonatomic, strong, readonly) NSString *country;
@end

@implementation KVCTest
- (instancetype)init {
    self = [super init];
    if (self) {
        _car = [Car new];
    }
    return self;
}
@end
