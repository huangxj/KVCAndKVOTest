//
//  KVOTestViewController.m
//  KVCKVOTest
//
//  Created by HuangXJ on 2017/6/10.
//  Copyright © 2017年 www.epoint.com.cn Inc. All rights reserved.
//

#import "KVOTestViewController.h"

@interface KVOTestViewController ()<UITabBarDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableV;
@property (nonatomic, strong) NSMutableArray *dataArr;
@end

static NSString *cellReuseID = @"UITableViewCell";
static int contextForKVO;
@implementation KVOTestViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    for (int i = 0; i < 100; i++) {
        NSString *str = [NSString stringWithFormat:@"%d", i];
        [self.dataArr addObject:str];
    }
    
    [self.tableV registerClass:[UITableViewCell class] forCellReuseIdentifier:cellReuseID];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.tableV addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionOld context:&contextForKVO];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (context == &contextForKVO) {
        if ([keyPath isEqualToString:@"contentOffset"]) {
            NSValue *oldValue = [change objectForKey:NSKeyValueChangeOldKey];
            // oldValue不一定存在
            if (oldValue) {
                CGPoint oldPoint = CGPointZero;
                [oldValue getValue:&oldPoint];
                NSLog(@"%@", NSStringFromCGPoint(oldPoint));
            }
            
            // 初始值和新值都以NSKeyValueChangeNewKey获取
            NSValue *newValue = [change objectForKey:NSKeyValueChangeNewKey];
            CGPoint newPoint = CGPointZero;
            [newValue getValue:&newPoint];
            NSLog(@"%@", NSStringFromCGPoint(newPoint));
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReuseID forIndexPath:indexPath];
    cell.textLabel.text = self.dataArr[indexPath.row];
    return cell;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (void)dealloc {
    [_tableV removeObserver:self forKeyPath:@"contentOffset" context:&contextForKVO];
}

//- (void)updateLastTime:(NSTimer *)t {
//    NSDate *now = [NSDate date];
//    [self willChangeValueForKey:@"lastTime"];
//    _lastTime = now;
//    [self didChangeValueForKey:@"lastTime"];
//}
@end
