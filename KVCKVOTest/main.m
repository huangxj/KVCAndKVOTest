//
//  main.m
//  KVCKVOTest
//
//  Created by HuangXJ on 2017/6/10.
//  Copyright © 2017年 www.epoint.com.cn Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
