/**
 * 作者：HuangXJ
 * 创建时间：2017/6/10 下午3:55
 */

#import <Foundation/Foundation.h>
@class Car;

@interface KVCTest : NSObject
/**
 姓名
 */
@property (nonatomic, strong) NSString *name;

/**
 年龄
 */
@property (nonatomic, assign) NSUInteger age;

@property (nonatomic, strong) Car *car;
@end
