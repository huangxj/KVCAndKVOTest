//
//  AppDelegate.h
//  KVCKVOTest
//
//  Created by HuangXJ on 2017/6/10.
//  Copyright © 2017年 www.epoint.com.cn Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

