# KVC
## KVC简介
KVC（key-value coding）能够让程序通过名称直接读取属性。因为与 KVC 有关的方法都是在 NSObject 中定义的，所以凡是继承自 NSOjbect 的类都具备 KVC 功能。

## KVC 执行过程简介
赋值方法：

```
[kvc setValue:@"huangxj" forKey:@"name"];
```
setValue:forKey:方法会查找名为 setName:的setter方法。如果对象kvc没有setName:方法，就会直接为实例变量赋值。

取值方法：

```
[kvc valueForKey:@"name"];
```
valueForKey:方法会先查找name的getter方法。如果没有定义该getter方法，就会直接返回相应实例变量的值。

## 非对象类型
KVC 只对对象有效，如果有些属性是非对象类型，例如int或float。可以使用NSNumber转换。
例如：

```
[kvc setValue:@27 forKey:@"age"];
NSNumber *age = [kvc valueForKey:@"age"];
NSLog(@"kvc.age = %zd", [age unsignedIntegerValue]);
```

## key路径
例如KVCTest有个属性Car，Car类有个属性为carColor(为了方便测试，设置为NSString类型)，如果要设置kvc实例变量中car的颜色属性。需要使用key路径，代码如下：

```
[kvc setValue:@"blue" forKeyPath:@"car.carColor"];
NSString *color = [kvc valueForKeyPath:@"car.carColor"];
NSLog(@"kvc.car.carColor = %@", color);
```

## 其它
使用 KVC 可以设置和读取.m中的私有属性和变量，也可以设置readOnly属性的值。

---
# KVO
## KVO 简介
KVO(key-value observing)是指当指定的对象的属性被修改时，允许对象接收通知的机制。KVO是Cocoa binding以及Core Data的关键组成部分。

## KVO 使用简介
以观察tableView的属性contentOffset为例

添加观察者

context参数一般使用静态变量的地址：static int contextForKVO;

关于context的详细说明参考下文。

```
- (void)viewDidLoad {
    [super viewDidLoad];    
    [self.tableV addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionOld context:&KVOContext];
}
```

获取值：

```
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (context == &KVOContext) {
        if ([keyPath isEqualToString:@"contentOffset"]) {
            NSValue *oldValue = [change objectForKey:NSKeyValueChangeOldKey];
            // oldValue不一定存在
            if (oldValue) {
                CGPoint oldPoint = CGPointZero;
                [oldValue getValue:&oldPoint];
                NSLog(@"%@", NSStringFromCGPoint(oldPoint));
            }
            
            // 初始值和新值都以NSKeyValueChangeNewKey获取
            NSValue *newValue = [change objectForKey:NSKeyValueChangeNewKey];
            CGPoint newPoint = CGPointZero;
            [newValue getValue:&newPoint];
            NSLog(@"%@", NSStringFromCGPoint(newPoint));
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}
```

释放观察者：

如果不做如下移除，控制器退出时会奔溃。

多次移除也会崩溃，尤其在当前类继承的父类也使用了观察者时，需要特别注意，这时可以通过context进行区和移除。

```
- (void)dealloc {
    [_tableV removeObserver:self forKeyPath:@"contentOffset" context:&contextForKVO];
}
```

## 在KVO中使用context
当在代码中将某个对象注册为观察者时，你需要传递指针作为context。当接收到变化的通知时，context会随通知一起发送。context可以用来区别收到的通知。例如，当父类也使用了KVO。当前类如果覆盖了observeValueForKeyPath:ofObject:context:方法，就需要通过context来区分并把父类观察者相应的消息转发给父类。context一般为静态变量的地址。

## 显示触发通知
如果使用存取方法来设置属性，那么系统会自动通知观察者。但如果出于某些原因，你选择不使用存取方法呢？这时可以通过willChangeValueForKey:和didChangeValueForKey:方法通知系统某个属性的值发生了变化。
例如：

```
- (void)updateLastTime:(NSTimer *)t {
    NSDate *now = [NSDate date];
    [self willChangeValueForKey:@"lastTime"];
    _lastTime = now;
    [self didChangeValueForKey:@"lastTime"];
}
```




